var date = 0;
var jsID1 = "aluuu-2-1446632758";
var jsID2 = "aluuu-1-1446632756"
var jsID3 = "aluuu-3-1446632758";
var baseHeight = 25;
var randomSpeed = 0;
var addSpeed = 40;
var baseScale = 60;

room.update = function()
{
	randomSpeed = (Math.random()/100);
	addSpeed += randomSpeed;
	room.objects[jsID1].pos.y = baseHeight + (Math.sin(addSpeed)/.2);
	room.objects[jsID1].scale.x = baseScale +(Math.sin(addSpeed)/.2);
	room.objects[jsID1].scale.y = baseScale +(Math.sin(addSpeed)/.2);
	room.objects[jsID1].scale.z = baseScale +(Math.sin(addSpeed)/.2);

	room.objects[jsID2].pos.y = baseHeight + (Math.sin(addSpeed)/20);
	room.objects[jsID2].scale.x = baseScale +(Math.sin(addSpeed)/20);
	room.objects[jsID2].scale.y = baseScale +(Math.sin(addSpeed)/20);
	room.objects[jsID2].scale.z = baseScale +(Math.sin(addSpeed)/20);

	room.objects[jsID3].pos.y = baseHeight + (Math.sin(addSpeed)/5);
	room.objects[jsID3].scale.x = baseScale +(Math.sin(addSpeed)/5);
	room.objects[jsID3].scale.y = baseScale +(Math.sin(addSpeed)/5);
	room.objects[jsID3].scale.z = baseScale +(Math.sin(addSpeed)/5);
}
